package freddy;

import freddy.num.Constants;
import freddy.num.Num;

/**
 * Created by romelldominguez on 6/14/17.
 */
public class Main {

    private static void method(Constants v){
        switch (v){
            case A:
                System.out.println(v.name());
                break;
            case B:
                System.out.println(v.ordinal());
                break;
            case C:
                System.out.println(v.toString());
                break;
        }
    }

    private static void method2(Num num){
        switch (num){
            case UNO:
                System.out.println(num);
                break;
            case DOS:
                System.out.println(num.getValue());
                break;
        }
    }

    private static void method3(Constants constant, Num num){
        if (constant.ordinal()==num.getValue()){
            System.out.println("same");
        } else System.out.println("Diff");
    }

    public static void main(String[] args) {
        method(Constants.C);
        method(Constants.A);
        method(Constants.B);
//        method();
        method2(Num.DOS);
        method2(Num.UNO);
        //
        method3(Constants.B,Num.UNO);
//        method3(Constants.B,new Num(2));
    }
}
