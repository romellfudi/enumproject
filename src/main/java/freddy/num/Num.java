package freddy.num;

/**
 * Created by romelldominguez on 6/14/17.
 */
public enum Num {
    UNO(1),DOS(2);

    private final int value;

    Num(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
